provider "aws" {
  region = "ap-south-1"
}

# terraform variables 

variable "vpc_cidr_block" { type = string }
variable "subnet_cidr_block" { type = string }
variable "envPrifix" { type = string }
variable "my-ip" { type = string }

# terraform data resourses

data "aws_region" "current" {}


# terraform resourses
resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "${var.envPrifix}-vpc"
  }
}

resource "aws_route_table" "myapp-rtb" {
  vpc_id = aws_vpc.myapp-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-gtw.id
  }

  tags = {
    Name = "${var.envPrifix}-rtb"
  }
}

resource "aws_internet_gateway" "myapp-gtw" {
  vpc_id = aws_vpc.myapp-vpc.id

  tags = {
    Name = "${var.envPrifix}-gtw"
  }
}

resource "aws_subnet" "myapp-subnet" {
  vpc_id            = aws_vpc.myapp-vpc.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = "${data.aws_region.current.name}a"

  tags = {
    Name = "${var.envPrifix}-subnet"
  }
}

resource "aws_route_table_association" "myapp_rtb_subnet_association" {
  subnet_id      = aws_subnet.myapp-subnet.id
  route_table_id = aws_route_table.myapp-rtb.id
}

resource "aws_security_group" "myapp-sg" {
  name   = "${var.envPrifix}-sg"
  vpc_id = aws_vpc.myapp-vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.my-ip}/32"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    "Name" = "${var.envPrifix}-sg"
  }
}

### instance resource

# get image id programatically from aws 

data "aws_ami" "image" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}


variable "instace-type" { type = string }
# variable "ssh-key-name" { type = string }

variable "public-key-location" { type = string }
resource "aws_key_pair" "ssh-key" {
  key_name   = "server-key"
  public_key = file(var.public-key-location)

}

resource "aws_instance" "myapp-server" {
  ami                         = data.aws_ami.image.id
  instance_type               = var.instace-type
  subnet_id                   = aws_subnet.myapp-subnet.id
  vpc_security_group_ids      = [aws_security_group.myapp-sg.id]
  availability_zone           = "${data.aws_region.current.name}a"
  associate_public_ip_address = true
  key_name                    = aws_key_pair.ssh-key.key_name
  tags = {
    "Name" = "${var.envPrifix}-server"
  }

}